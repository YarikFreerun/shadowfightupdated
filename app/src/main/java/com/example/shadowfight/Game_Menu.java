package com.example.shadowfight;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

public class Game_Menu extends Activity {

    private MediaPlayer mpbg;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game__menu);

        mpbg = MediaPlayer.create(this, R.raw.menu);
        mpbg.setLooping(true);
        mpbg.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_game__menu, menu);
        return true;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_start: {
                Intent intent = new Intent(Game_Menu.this, GameProcess.class);
                Game_Menu.this.startActivity(intent);
                break;
            }
            case R.id.btn_about: {
                String text = "Term Project: Shadow Fight. Student: Yaroslav Berezanskyi IACS 204.All rights " +
                              "reserved, Kyiv 2012";
                Toast.makeText(this, text, Toast.LENGTH_LONG).show();
                break;
            }
        }
    }

    public void onPause() {
        super.onPause();
        mpbg.stop();
    }
}
