package com.example.shadowfight;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Environment;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

import java.io.IOException;
import java.util.Random;

public class Entities extends View {

    private boolean theEnd = false, theWin = false, theLose = false, theFinish = false;

    private Paint paint;
    private int x = 250, y = 320;
    private int x_Enemy = 800, y_Enemy = 320;
    private boolean ORIENTATION = true;
    private String  STATE       = "PASSIVE";
    private String  STATE_Enemy = "PASSIVE";
    private double t, t_Enemy;

    private int generator = 0;
    private Random r;

    private Context context;

    private boolean kicked = false, kicked_Enemy = false;
    private boolean impPhase = false, impPhase_Enemy = false;

    private int healthFactor = 0, healthFactor_Enemy = 0;

    private int stepCount = 0, passiveCount = 0, stepbackCount = 0, dodgeCount = 0, handCount = 0, closehandCount =
            0, directhandCount = 0, legCount = 0, directlegCount = 0, closelegCount = 0, wheelCount = 0;
    private int stepCount_R = 0, passiveCount_R = 0, stepbackCount_R = 0, dodgeCount_R = 0, handCount_R = 0,
            closehandCount_R = 0, directhandCount_R = 0, legCount_R = 0, directlegCount_R = 0, closelegCount_R = 0,
            wheelCount_R = 0;

    private int passiveCount_Enemy = 0, stepCount_Enemy = 0, stepbackCount_Enemy = 0, dodgeCount_Enemy = 0,
            handCount_Enemy = 0, closehandCount_Enemy = 0, directhandCount_Enemy = 0, legCount_Enemy = 0,
            directlegCount_Enemy = 0, closelegCount_Enemy = 0, wheelCount_Enemy = 0;
    private int passiveCount_Enemy_R = 0, stepCount_Enemy_R = 0, stepbackCount_Enemy_R = 0, dodgeCount_Enemy_R = 0,
            handCount_Enemy_R = 0, closehandCount_Enemy_R = 0, directhandCount_Enemy_R = 0, legCount_Enemy_R = 0,
            directlegCount_Enemy_R = 0, closelegCount_Enemy_R = 0, wheelCount_Enemy_R = 0;

    private float                canvasSize;
    private ScaleGestureDetector scaleGestureDetector;
    private int                  viewSize;
    private float                mScaleFactor;

    private GestureDetector detector;

    private Vibrator vibrator;

    private MediaPlayer mediaPlayer, mp1, mp2, mp3;
    private MediaPlayer mp;

    private Bitmap   BG;
    private Bitmap[] PASSIVE;
    private Bitmap[] STEP;
    private Bitmap[] STEPBACK;
    private Bitmap[] DODGE;
    private Bitmap[] HAND;
    private Bitmap[] CLOSEHAND;
    private Bitmap[] DIRECTHAND;
    private Bitmap[] LEG;
    private Bitmap[] DIRECTLEG;
    private Bitmap[] CLOSELEG;
    private Bitmap[] WHEEL;
    private Bitmap   DEATH;
    private Bitmap   DONE;

    private Bitmap[] PASSIVE_R;
    private Bitmap[] STEP_R;
    private Bitmap[] STEPBACK_R;
    private Bitmap[] DODGE_R;
    private Bitmap[] HAND_R;
    private Bitmap[] CLOSEHAND_R;
    private Bitmap[] DIRECTHAND_R;
    private Bitmap[] LEG_R;
    private Bitmap[] DIRECTLEG_R;
    private Bitmap[] CLOSELEG_R;
    private Bitmap[] WHEEL_R;
    private Bitmap   DEATH_R;
    private Bitmap   DONE_R;

    private Bitmap[] HEALTH;
    private Bitmap[] HEALTH_Enemy;
    private Bitmap   DEATHSCREEN;

    private final String passive    = "PASSIVE";
    private final String step       = "STEP";
    private final String stepback   = "STEPBACK";
    private final String dodge      = "DODGE";
    private final String hand       = "HAND";
    private final String closehand  = "CLOSEHAND";
    private final String directhand = "DIRECTHAND";
    private final String leg        = "LEG";
    private final String directleg  = "DIRECTLEG";
    private final String closeleg   = "CLOSELEG";
    private final String wheel      = "WHEEL";
    private final String death      = "DEATH";
    private final String done       = "DONE";

    public Entities(Context context, AttributeSet attrs, Vibrator vibrator) {
        super(context, attrs);

        String DOWNLOADS = null;
        try {
            DOWNLOADS = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                    .getCanonicalPath();
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.context = context;

        this.vibrator = vibrator;
        mediaPlayer = MediaPlayer.create(context, R.raw.orientation);
        mp = MediaPlayer.create(context, R.raw.zria);
        mp.setVolume(100, 100);

        mp1 = MediaPlayer.create(context, R.raw.mkexcellent);
        mp2 = MediaPlayer.create(context, R.raw.mkfinishhim);
        mp3 = MediaPlayer.create(context, R.raw.mksklaugh);

        viewSize = (int) convertDpToPixel(320, context);
        mScaleFactor = 1f;
        canvasSize = (int) (viewSize * mScaleFactor);
        scaleGestureDetector = new ScaleGestureDetector(context, new MyScaleGestureListener());
        detector = new GestureDetector(context, new MyGestureListener());

        t = System.currentTimeMillis();

        r = new Random();

        int i = 0;
        BG = BitmapFactory.decodeFile(DOWNLOADS + "/3.jpg");

        paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.RED);
        paint.setTextSize(40);
        paint.setAntiAlias(true);
        paint.setTypeface(Typeface.MONOSPACE);

        PASSIVE = new Bitmap[21];
        for (i = 0; i <= 20; i++) {
            PASSIVE[i] = BitmapFactory.decodeFile(DOWNLOADS + "/Sprites/PASSIVE/" + Integer.toString(i) + ".png");
        }

        STEP = new Bitmap[3];
        for (i = 0; i <= 2; i++) {
            STEP[i] = BitmapFactory.decodeFile(DOWNLOADS + "/Sprites/STEP/" + Integer.toString(i) + ".png");
        }

        STEPBACK = new Bitmap[4];
        for (i = 0; i <= 3; i++) {
            STEPBACK[i] = BitmapFactory.decodeFile(DOWNLOADS + "/Sprites/STEPBACK/" + Integer.toString(i) + ".png");
        }

        DODGE = new Bitmap[6];
        for (i = 0; i <= 5; i++) {
            DODGE[i] = BitmapFactory.decodeFile(DOWNLOADS + "/Sprites/DODGE/" + Integer.toString(i) + ".png");
        }

        HEALTH = new Bitmap[28];
        for (i = 0; i <= 27; i++) {
            HEALTH[i] = BitmapFactory.decodeFile(DOWNLOADS + "/Sprites/HEALTH/" + Integer.toString(i) + ".png");
        }

        HEALTH_Enemy = new Bitmap[28];
        for (i = 0; i <= 27; i++) {
            HEALTH_Enemy[i] = BitmapFactory.decodeFile(
                    DOWNLOADS + "/Sprites/HEALTH_Enemy/" + Integer.toString(i) + ".png");
        }

        HAND = new Bitmap[6];
        for (i = 0; i <= 5; i++) {
            HAND[i] = BitmapFactory.decodeFile(DOWNLOADS + "/Sprites/HANDS/HAND/" + Integer.toString(i) + ".png");
        }

        CLOSEHAND = new Bitmap[3];
        for (i = 0; i <= 2; i++) {
            CLOSEHAND[i] = BitmapFactory.decodeFile(
                    DOWNLOADS + "/Sprites/HANDS/CLOSEHAND/" + Integer.toString(i) + ".png");
        }

        DIRECTHAND = new Bitmap[3];
        for (i = 0; i <= 2; i++) {
            DIRECTHAND[i] = BitmapFactory.decodeFile(
                    DOWNLOADS + "/Sprites/HANDS/DIRECTHAND/" + Integer.toString(i) + ".png");
        }

        LEG = new Bitmap[6];
        for (i = 0; i <= 5; i++) {
            LEG[i] = BitmapFactory.decodeFile(DOWNLOADS + "/Sprites/LEGS/LEG/" + Integer.toString(i) + ".png");
        }

        DIRECTLEG = new Bitmap[3];
        for (i = 0; i <= 2; i++) {
            DIRECTLEG[i] = BitmapFactory.decodeFile(
                    DOWNLOADS + "/Sprites/LEGS/DIRECTLEG/" + Integer.toString(i) + ".png");
        }

        CLOSELEG = new Bitmap[3];
        for (i = 0; i <= 2; i++) {
            CLOSELEG[i] = BitmapFactory.decodeFile(
                    DOWNLOADS + "/Sprites/LEGS/CLOSELEG/" + Integer.toString(i) + ".png");
        }

        WHEEL = new Bitmap[8];
        for (i = 0; i <= 7; i++) {
            WHEEL[i] = BitmapFactory.decodeFile(DOWNLOADS + "/Sprites/WHEEL/" + Integer.toString(i) + ".png");
        }

        ////////////////////////////////////////////////////

        PASSIVE_R = new Bitmap[21];
        for (i = 0; i <= 20; i++) {
            PASSIVE_R[i] = BitmapFactory.decodeFile(DOWNLOADS + "/Sprites/PASSIVE_R/" + Integer.toString(i) + ".png");
        }

        STEP_R = new Bitmap[3];
        for (i = 0; i <= 2; i++) {
            STEP_R[i] = BitmapFactory.decodeFile(DOWNLOADS + "/Sprites/STEP_R/" + Integer.toString(i) + ".png");
        }

        STEPBACK_R = new Bitmap[4];
        for (i = 0; i <= 3; i++) {
            STEPBACK_R[i] = BitmapFactory.decodeFile(DOWNLOADS + "/Sprites/STEPBACK_R/" + Integer.toString(i) + ".png");
        }

        DODGE_R = new Bitmap[6];
        for (i = 0; i <= 5; i++) {
            DODGE_R[i] = BitmapFactory.decodeFile(DOWNLOADS + "/Sprites/DODGE_R/" + Integer.toString(i) + ".png");
        }

        HAND_R = new Bitmap[6];
        for (i = 0; i <= 5; i++) {
            HAND_R[i] = BitmapFactory.decodeFile(DOWNLOADS + "/Sprites/HANDS/HAND_R/" + Integer.toString(i) + ".png");
        }

        CLOSEHAND_R = new Bitmap[3];
        for (i = 0; i <= 2; i++) {
            CLOSEHAND_R[i] = BitmapFactory.decodeFile(
                    DOWNLOADS + "/Sprites/HANDS/CLOSEHAND_R/" + Integer.toString(i) + ".png");
        }

        WHEEL_R = new Bitmap[8];
        for (i = 0; i <= 7; i++) {
            WHEEL_R[i] = BitmapFactory.decodeFile(DOWNLOADS + "/Sprites/WHEEL_R/" + Integer.toString(i) + ".png");
        }

        DIRECTHAND_R = new Bitmap[3];
        for (i = 0; i <= 2; i++) {
            DIRECTHAND_R[i] = BitmapFactory.decodeFile(
                    DOWNLOADS + "/Sprites/HANDS/DIRECTHAND_R/" + Integer.toString(i) + ".png");
        }

        LEG_R = new Bitmap[6];
        for (i = 0; i <= 5; i++) {
            LEG_R[i] = BitmapFactory.decodeFile(DOWNLOADS + "/Sprites/LEGS/LEG_R/" + Integer.toString(i) + ".png");
        }

        DIRECTLEG_R = new Bitmap[3];
        for (i = 0; i <= 2; i++) {
            DIRECTLEG_R[i] = BitmapFactory.decodeFile(
                    DOWNLOADS + "/Sprites/LEGS/DIRECTLEG_R/" + Integer.toString(i) + ".png");
        }

        CLOSELEG_R = new Bitmap[3];
        for (i = 0; i <= 2; i++) {
            CLOSELEG_R[i] = BitmapFactory.decodeFile(
                    DOWNLOADS + "/Sprites/LEGS/CLOSELEG_R/" + Integer.toString(i) + ".png");
        }

        DEATH = BitmapFactory.decodeFile(DOWNLOADS + "/Sprites/DEATH/0.png");
        DEATH_R = BitmapFactory.decodeFile(DOWNLOADS + "/Sprites/DEATH_R/0.png");

        DONE = BitmapFactory.decodeFile(DOWNLOADS + "/Sprites/DONE/0.png");
        DONE_R = BitmapFactory.decodeFile(DOWNLOADS + "/Sprites/DONE_R/0.png");

        DEATHSCREEN = BitmapFactory.decodeFile(DOWNLOADS + "/Sprites/DEATH.png");
    }

    public boolean onTouchEvent(MotionEvent event) {

        detector.onTouchEvent(event);
        scaleGestureDetector.onTouchEvent(event);

        if (!impPhase) {

            if (event.getAction() == MotionEvent.ACTION_UP) {

                if (event.getY() >= y + 250) {

                    if (event.getX() <= x - 40) {
                        if (ORIENTATION) STATE = stepback;
                        else STATE = step;
                    }

                    if ((event.getX() > x - 40) && (event.getX() < x + 170 + 40)) {
                        STATE = dodge;
                    }

                    if (event.getX() > x + 170 + 40) {
                        if (ORIENTATION) STATE = step;
                        else STATE = stepback;
                    }
                }

                if (event.getX() <= x + 80) {

                    if ((event.getY() > y - 40) && (event.getY() <= y + 60)) {
                        if (ORIENTATION) STATE = hand;
                        else STATE = leg;
                    }

                    if ((event.getY() > y + 60) && (event.getY() <= y + 60 + 100)) {
                        if (ORIENTATION) STATE = directhand;
                        else STATE = directleg;
                    }

                    if ((event.getY() > y + 60 + 100) && (event.getY() < y + 250)) {
                        if (ORIENTATION) STATE = closehand;
                        else STATE = closeleg;
                    }
                } else if (event.getX() > x + 80) {

                    if ((event.getY() > y - 40) && (event.getY() <= y + 60)) {
                        if (ORIENTATION) STATE = leg;
                        else STATE = hand;
                    }

                    if ((event.getY() > y + 60) && (event.getY() <= y + 60 + 100)) {
                        if (ORIENTATION) STATE = directleg;
                        else STATE = directhand;
                    }

                    if ((event.getY() > y + 60 + 100) && (event.getY() < y + 250)) {
                        if (ORIENTATION) STATE = closeleg;
                        else STATE = closehand;
                    }
                }

                if (event.getY() <= y - 40) {
                    STATE = wheel;
                }
            }
        }
        return true;
    }

    protected void onUpdate() {

        if (healthFactor > 27) {
            STATE = death;
            theEnd = true;
            if (!theWin) {
                mp3.start();
                theWin = true;
            }
        }

        if (y_Enemy < 0) STATE = done;

        if (x - 100 > x_Enemy) {
            ORIENTATION = false;
        } else {
            if (x_Enemy - 100 > x) {
                ORIENTATION = true;
            }
        }

        if (STATE == dodge || STATE == wheel) impPhase = true;
        else impPhase = false;

        if (STATE == passive) {
            if ((System.currentTimeMillis() - t) / 1000 > 0.2) {
                if (ORIENTATION) {
                    passiveCount = passiveCount + 1;
                } else {
                    passiveCount_R = passiveCount_R + 1;
                }
                t = System.currentTimeMillis();
            }
            if (passiveCount > 20) {
                passiveCount = 0;
                STATE = passive;
            }
            if (passiveCount_R > 20) {
                passiveCount_R = 0;
                STATE = passive;
            }
        }

        if (STATE == step) {
            if ((System.currentTimeMillis() - t) / 1000 > 0.15) {
                if (ORIENTATION) {
                    stepCount = stepCount + 1;
                    x = x + 18;
                } else {
                    stepCount_R = stepCount_R + 1;
                    x = x - 18;
                }
                t = System.currentTimeMillis();
            }
            if (stepCount > 2) {
                stepCount = 0;
                STATE = passive;
            }
            if (stepCount_R > 2) {
                stepCount_R = 0;
                STATE = passive;
            }
        }

        if (STATE == stepback) {
            if ((System.currentTimeMillis() - t) / 1000 > 0.15) {
                if (ORIENTATION) {
                    stepbackCount = stepbackCount + 1;
                    x = x - 15;
                } else {
                    stepbackCount_R = stepbackCount_R + 1;
                    x = x + 15;
                }
                t = System.currentTimeMillis();
            }
            if (stepbackCount > 3) {
                stepbackCount = 0;
                STATE = passive;
            }
            if (stepbackCount_R > 3) {
                stepbackCount_R = 0;
                STATE = passive;
            }
        }

        if (STATE == dodge) {
            if ((System.currentTimeMillis() - t) / 1000 > 0.1) {
                if (ORIENTATION) {
                    dodgeCount = dodgeCount + 1;
                } else {
                    dodgeCount_R = dodgeCount_R + 1;
                }
                t = System.currentTimeMillis();
            }

            if (dodgeCount > 5) {
                dodgeCount = 0;
                STATE = passive;
            }
            if (dodgeCount_R > 5) {
                dodgeCount_R = 0;
                STATE = passive;
            }
        }

        if (STATE == hand) {
            if ((System.currentTimeMillis() - t) / 1000 > 0.1) {
                if (ORIENTATION) {
                    handCount = handCount + 1;
                } else {
                    handCount_R = handCount_R + 1;
                }
                t = System.currentTimeMillis();
            }
            if (handCount > 5) {
                handCount = 0;
                STATE = passive;
                kicked = false;
            }
            if (handCount_R > 5) {
                handCount_R = 0;
                STATE = passive;
                kicked = false;
            }
        }

        if (STATE == closehand) {
            if ((System.currentTimeMillis() - t) / 1000 > 0.2) {
                if (ORIENTATION) {
                    closehandCount = closehandCount + 1;
                } else {
                    closehandCount_R = closehandCount_R + 1;
                }
                t = System.currentTimeMillis();
            }
            if (closehandCount > 2) {
                closehandCount = 0;
                STATE = passive;
                kicked = false;
            }
            if (closehandCount_R > 2) {
                closehandCount_R = 0;
                STATE = passive;
                kicked = false;
            }
        }

        if (STATE == directhand) {
            if ((System.currentTimeMillis() - t) / 1000 > 0.12) {
                if (ORIENTATION) {
                    directhandCount = directhandCount + 1;
                    x = x + 2;
                } else {
                    directhandCount_R = directhandCount_R + 1;
                    x = x - 2;
                }
                t = System.currentTimeMillis();
            }
            if (directhandCount > 2) {
                directhandCount = 0;
                STATE = passive;
                kicked = false;
            }
            if (directhandCount_R > 2) {
                directhandCount_R = 0;
                STATE = passive;
                kicked = false;
            }
        }

        if (STATE == leg) {
            if ((System.currentTimeMillis() - t) / 1000 > 0.1) {
                if (ORIENTATION) {
                    legCount = legCount + 1;
                } else {
                    legCount_R = legCount_R + 1;
                }
                t = System.currentTimeMillis();
            }
            if (legCount > 5) {
                legCount = 0;
                STATE = passive;
                kicked = false;
            }
            if (legCount_R > 5) {
                legCount_R = 0;
                STATE = passive;
                kicked = false;
            }
        }

        if (STATE == directleg) {
            if ((System.currentTimeMillis() - t) / 1000 > 0.2) {
                if (ORIENTATION) {
                    directlegCount = directlegCount + 1;
                } else {
                    directlegCount_R = directlegCount_R + 1;
                }
                t = System.currentTimeMillis();
            }

            if (directlegCount > 2) {
                directlegCount = 0;
                STATE = passive;
                kicked = false;
            }
            if (directlegCount_R > 2) {
                directlegCount_R = 0;
                STATE = passive;
                kicked = false;
            }
        }

        if (STATE == closeleg) {
            if ((System.currentTimeMillis() - t) / 1000 > 0.18) {
                if (ORIENTATION) {
                    closelegCount = closelegCount + 1;
                } else {
                    closelegCount_R = closelegCount_R + 1;
                }
                t = System.currentTimeMillis();
            }
            if (closelegCount > 2) {
                closelegCount = 0;
                STATE = passive;
                kicked = false;
            }
            if (closelegCount_R > 2) {
                closelegCount_R = 0;
                STATE = passive;
                kicked = false;
            }
        }

        if (STATE == wheel) {
            if ((System.currentTimeMillis() - t) / 1000 > 0.15) {
                if (ORIENTATION) {
                    wheelCount = wheelCount + 1;
                    x = x + 15;
                } else {
                    wheelCount_R = wheelCount_R + 1;
                    x = x - 15;
                }
                t = System.currentTimeMillis();
            }
            if (wheelCount > 7) {
                wheelCount = 0;
                STATE = passive;
                x = x + 25;
                kicked = false;
            }
            if (wheelCount_R > 7) {
                wheelCount_R = 0;
                STATE = passive;
                x = x + 25;
                kicked = false;
            }
        }

        if (STATE == death) {
            if ((System.currentTimeMillis() - t) / 1000 > 0.05) {
                if (ORIENTATION) {
                    x = x - 60;
                    y = y - 60;
                } else {
                    x = x + 60;
                    y = y - 60;
                }
                t = System.currentTimeMillis();
            }
        }
    }

    protected void onDraw(Canvas canvas) {

        if (!kicked) catchCollisions();
        if (!kicked_Enemy) catchCollisions_Enemy();

        updatePhases(STATE);
        onUpdate();
        onUpdateEnemy();

        canvas.save();

        canvas.scale(mScaleFactor, mScaleFactor);

        canvas.drawBitmap(BG, 0, 0, null);
        if (STATE != death) canvas.drawBitmap(HEALTH[healthFactor], 0, 0, null);
        if (STATE_Enemy != death) canvas.drawBitmap(HEALTH_Enemy[healthFactor_Enemy], 700, 0, null);
        onDrawEnemy(canvas);

        canvas.drawText("YARIK", x + 10, y - 40, paint);

        if (STATE == passive) {
            if (ORIENTATION) {
                canvas.drawBitmap(PASSIVE[passiveCount], x, y, null);
            } else {
                canvas.drawBitmap(PASSIVE_R[passiveCount_R], x, y, null);
            }
        }

        if (STATE == step) {
            if (ORIENTATION) {
                canvas.drawBitmap(STEP[stepCount], x, y, null);
            } else {
                canvas.drawBitmap(STEP_R[stepCount_R], x, y, null);
            }
        }

        if (STATE == stepback) {
            if (ORIENTATION) {
                canvas.drawBitmap(STEPBACK[stepbackCount], x, y, null);
            } else {
                canvas.drawBitmap(STEPBACK_R[stepbackCount_R], x, y, null);
            }
        }

        if (STATE == dodge) {
            if (ORIENTATION) {
                canvas.drawBitmap(DODGE[dodgeCount], x, y, null);
            } else {
                canvas.drawBitmap(DODGE_R[dodgeCount_R], x, y, null);
            }
        }

        if (STATE == hand) {
            if (ORIENTATION) {
                canvas.drawBitmap(HAND[handCount], x, y, null);
            } else {
                canvas.drawBitmap(HAND_R[handCount_R], x, y, null);
            }
        }

        if (STATE == closehand) {
            if (ORIENTATION) {
                canvas.drawBitmap(CLOSEHAND[closehandCount], x, y - 4, null);
            } else {
                canvas.drawBitmap(CLOSEHAND_R[closehandCount_R], x, y - 4, null);
            }
        }

        if (STATE == directhand) {
            if (ORIENTATION) {
                canvas.drawBitmap(DIRECTHAND[directhandCount], x, y - 4, null);
            } else {
                canvas.drawBitmap(DIRECTHAND_R[directhandCount_R], x, y - 4, null);
            }
        }

        if (STATE == leg) {
            if (ORIENTATION) {
                canvas.drawBitmap(LEG[legCount], x, y - 25, null);
            } else {
                canvas.drawBitmap(LEG_R[legCount_R], x, y - 25, null);
            }
        }

        if (STATE == directleg) {
            if (ORIENTATION) {
                canvas.drawBitmap(DIRECTLEG[directlegCount], x, y - 20, null);
            } else {
                canvas.drawBitmap(DIRECTLEG_R[directlegCount_R], x, y - 20, null);
            }
        }

        if (STATE == closeleg) {
            if (ORIENTATION) {
                canvas.drawBitmap(CLOSELEG[closelegCount], x, y - 27, null);
            } else {
                canvas.drawBitmap(CLOSELEG_R[closelegCount_R], x, y - 27, null);
            }
        }

        if (STATE == wheel) {
            if (ORIENTATION) {
                canvas.drawBitmap(WHEEL[wheelCount], x, y - 50, null);
            } else {
                canvas.drawBitmap(WHEEL_R[wheelCount_R], x, y - 50, null);
            }
        }

        if (STATE == death) {
            if (ORIENTATION) {
                canvas.drawBitmap(DEATH, x, y, null);
            } else {
                canvas.drawBitmap(DEATH_R, x, y, null);
            }
        }

        if (STATE == done) {
            if (ORIENTATION) {
                canvas.drawBitmap(DONE, x, y, null);
            } else {
                canvas.drawBitmap(DONE_R, x, y, null);
            }
        }
        if (STATE == death || STATE_Enemy == death) canvas.drawBitmap(DEATHSCREEN, 0, 0, null);

        canvas.restore();
        this.invalidate();
    }

    protected void onDrawEnemy(Canvas canvas) {

        if (STATE_Enemy == passive) {
            if (!ORIENTATION) {
                canvas.drawBitmap(PASSIVE[passiveCount_Enemy], x_Enemy, y_Enemy, null);
            } else {
                canvas.drawBitmap(PASSIVE_R[passiveCount_Enemy_R], x_Enemy, y_Enemy, null);
            }
        }

        if (STATE_Enemy == step) {
            if (!ORIENTATION) {
                canvas.drawBitmap(STEP[stepCount_Enemy], x_Enemy, y_Enemy, null);
            } else {
                canvas.drawBitmap(STEP_R[stepCount_Enemy_R], x_Enemy, y_Enemy, null);
            }
        }

        if (STATE_Enemy == stepback) {
            if (!ORIENTATION) {
                canvas.drawBitmap(STEPBACK[stepbackCount_Enemy], x_Enemy, y_Enemy, null);
            } else {
                canvas.drawBitmap(STEPBACK_R[stepbackCount_Enemy_R], x_Enemy, y_Enemy, null);
            }
        }

        if (STATE_Enemy == dodge) {
            if (!ORIENTATION) {
                canvas.drawBitmap(DODGE[dodgeCount_Enemy], x_Enemy, y_Enemy, null);
            } else {
                canvas.drawBitmap(DODGE_R[dodgeCount_Enemy_R], x_Enemy, y_Enemy, null);
            }
        }

        if (STATE_Enemy == hand) {
            if (!ORIENTATION) {
                canvas.drawBitmap(HAND[handCount_Enemy], x_Enemy, y_Enemy, null);
            } else {
                canvas.drawBitmap(HAND_R[handCount_Enemy_R], x_Enemy, y_Enemy, null);
            }
        }

        if (STATE_Enemy == closehand) {
            if (!ORIENTATION) {
                canvas.drawBitmap(CLOSEHAND[closehandCount_Enemy], x_Enemy, y_Enemy - 4, null);
            } else {
                canvas.drawBitmap(CLOSEHAND_R[closehandCount_Enemy_R], x_Enemy, y_Enemy - 4, null);
            }
        }

        if (STATE_Enemy == directhand) {
            if (!ORIENTATION) {
                canvas.drawBitmap(DIRECTHAND[directhandCount_Enemy], x_Enemy, y_Enemy - 4, null);
            } else {
                canvas.drawBitmap(DIRECTHAND_R[directhandCount_Enemy_R], x_Enemy, y_Enemy - 4, null);
            }
        }

        if (STATE_Enemy == leg) {
            if (!ORIENTATION) {
                canvas.drawBitmap(LEG[legCount_Enemy], x_Enemy, y_Enemy - 25, null);
            } else {
                canvas.drawBitmap(LEG_R[legCount_Enemy_R], x_Enemy, y_Enemy - 25, null);
            }
        }

        if (STATE_Enemy == directleg) {
            if (!ORIENTATION) {
                canvas.drawBitmap(DIRECTLEG[directlegCount_Enemy], x_Enemy, y_Enemy - 20, null);
            } else {
                canvas.drawBitmap(DIRECTLEG_R[directlegCount_Enemy_R], x_Enemy, y_Enemy - 20, null);
            }
        }

        if (STATE_Enemy == closeleg) {
            if (!ORIENTATION) {
                canvas.drawBitmap(CLOSELEG[closelegCount_Enemy], x_Enemy, y_Enemy - 27, null);
            } else {
                canvas.drawBitmap(CLOSELEG_R[closelegCount_Enemy_R], x_Enemy, y_Enemy - 27, null);
            }
        }

        if (STATE_Enemy == wheel) {
            if (!ORIENTATION) {
                canvas.drawBitmap(WHEEL[wheelCount_Enemy], x_Enemy, y_Enemy - 50, null);
            } else {
                canvas.drawBitmap(WHEEL_R[wheelCount_Enemy_R], x_Enemy, y_Enemy - 50, null);
            }
        }

        if (STATE_Enemy == death) {
            if (!ORIENTATION) {
                canvas.drawBitmap(DEATH, x_Enemy, y_Enemy, null);
            } else {
                canvas.drawBitmap(DEATH_R, x_Enemy, y_Enemy, null);
            }
        }
        if (STATE_Enemy == done) {
            if (!ORIENTATION) {
                canvas.drawBitmap(DONE, x_Enemy, y_Enemy, null);
            } else {
                canvas.drawBitmap(DONE_R, x_Enemy, y_Enemy, null);
            }
        }
    }

    protected void onUpdateEnemy() {

        if (healthFactor_Enemy > 27) {
            STATE_Enemy = death;
            theEnd = true;
            if (!theLose) {
                mp1.start();
                theLose = true;
            }
        }

        if (healthFactor_Enemy == 27) if (!theFinish) {
            mp2.start();
            theFinish = true;
        }

        if (!impPhase_Enemy) generator();

        if (y < 0) STATE_Enemy = done;

        if (STATE_Enemy == passive) {
            if ((System.currentTimeMillis() - t_Enemy) / 1000 > 0.2) {
                if (!ORIENTATION) {
                    passiveCount_Enemy = passiveCount_Enemy + 1;
                } else {
                    passiveCount_Enemy_R = passiveCount_Enemy_R + 1;
                }
                t_Enemy = System.currentTimeMillis();
            }
            if (passiveCount_Enemy > 20) {
                passiveCount_Enemy = 0;
                STATE_Enemy = passive;
                kicked_Enemy = false;
            }
            if (passiveCount_Enemy_R > 20) {
                passiveCount_Enemy_R = 0;
                STATE_Enemy = passive;
                kicked_Enemy = false;
            }
        }

        if (STATE_Enemy == step) {
            if ((System.currentTimeMillis() - t_Enemy) / 1000 > 0.2) {
                if (!ORIENTATION) {
                    stepCount_Enemy = stepCount_Enemy + 1;
                    x_Enemy = x_Enemy + 18;
                } else {
                    stepCount_Enemy_R = stepCount_Enemy_R + 1;
                    x_Enemy = x_Enemy - 18;
                }
                t_Enemy = System.currentTimeMillis();
            }
            if (stepCount_Enemy > 2) {
                stepCount_Enemy = 0;
                STATE_Enemy = passive;
                impPhase_Enemy = false;
                kicked_Enemy = false;
            }
            if (stepCount_Enemy_R > 2) {
                stepCount_Enemy_R = 0;
                STATE_Enemy = passive;
                impPhase_Enemy = false;
                kicked_Enemy = false;
            }
        }

        if (STATE_Enemy == stepback) {
            if ((System.currentTimeMillis() - t_Enemy) / 1000 > 0.15) {
                if (!ORIENTATION) {
                    stepbackCount_Enemy = stepbackCount_Enemy + 1;
                    x_Enemy = x_Enemy - 15;
                } else {
                    stepbackCount_Enemy_R = stepbackCount_Enemy_R + 1;
                    x_Enemy = x_Enemy + 15;
                }
                t_Enemy = System.currentTimeMillis();
            }
            if (stepbackCount_Enemy > 3) {
                stepbackCount_Enemy = 0;
                STATE_Enemy = passive;
                impPhase_Enemy = false;
                kicked_Enemy = false;
            }
            if (stepbackCount_Enemy_R > 3) {
                stepbackCount_Enemy_R = 0;
                STATE_Enemy = passive;
                impPhase_Enemy = false;
                kicked_Enemy = false;
            }
        }

        if (STATE_Enemy == dodge) {
            if ((System.currentTimeMillis() - t_Enemy) / 1000 > 0.1) {
                if (!ORIENTATION) {
                    dodgeCount_Enemy = dodgeCount_Enemy + 1;
                } else {
                    dodgeCount_Enemy_R = dodgeCount_Enemy_R + 1;
                }
                t_Enemy = System.currentTimeMillis();
            }

            if (dodgeCount_Enemy > 5) {
                dodgeCount_Enemy = 0;
                STATE_Enemy = passive;
                impPhase_Enemy = false;
                kicked_Enemy = false;
            }
            if (dodgeCount_Enemy_R > 5) {
                dodgeCount_Enemy_R = 0;
                STATE_Enemy = passive;
                impPhase_Enemy = false;
                kicked_Enemy = false;
            }
        }

        if (STATE_Enemy == hand) {
            if ((System.currentTimeMillis() - t_Enemy) / 1000 > 0.1) {
                if (!ORIENTATION) {
                    handCount_Enemy = handCount_Enemy + 1;
                } else {
                    handCount_Enemy_R = handCount_Enemy_R + 1;
                }
                t_Enemy = System.currentTimeMillis();
            }
            if (handCount_Enemy > 5) {
                handCount_Enemy = 0;
                STATE_Enemy = passive;
                impPhase_Enemy = false;
                kicked_Enemy = false;
            }
            if (handCount_Enemy_R > 5) {
                handCount_Enemy_R = 0;
                STATE_Enemy = passive;
                impPhase_Enemy = false;
                kicked_Enemy = false;
            }
        }

        if (STATE_Enemy == closehand) {
            if ((System.currentTimeMillis() - t_Enemy) / 1000 > 0.2) {
                if (!ORIENTATION) {
                    closehandCount_Enemy = closehandCount_Enemy + 1;
                } else {
                    closehandCount_Enemy_R = closehandCount_Enemy_R + 1;
                }
                t_Enemy = System.currentTimeMillis();
            }
            if (closehandCount_Enemy > 2) {
                closehandCount_Enemy = 0;
                STATE_Enemy = passive;
                impPhase_Enemy = false;
                kicked_Enemy = false;
            }
            if (closehandCount_Enemy_R > 2) {
                closehandCount_Enemy_R = 0;
                STATE_Enemy = passive;
                impPhase_Enemy = false;
                kicked_Enemy = false;
            }
        }

        if (STATE_Enemy == directhand) {
            if ((System.currentTimeMillis() - t_Enemy) / 1000 > 0.12) {
                if (!ORIENTATION) {
                    directhandCount_Enemy = directhandCount_Enemy + 1;
                    x_Enemy = x_Enemy + 2;
                } else {
                    directhandCount_Enemy_R = directhandCount_Enemy_R + 1;
                    x_Enemy = x_Enemy - 2;
                }
                t_Enemy = System.currentTimeMillis();
            }
            if (directhandCount_Enemy > 2) {
                directhandCount_Enemy = 0;
                STATE_Enemy = passive;
                impPhase_Enemy = false;
                kicked_Enemy = false;
            }
            if (directhandCount_Enemy_R > 2) {
                directhandCount_Enemy_R = 0;
                STATE_Enemy = passive;
                impPhase_Enemy = false;
                kicked_Enemy = false;
            }
        }

        if (STATE_Enemy == leg) {
            if ((System.currentTimeMillis() - t_Enemy) / 1000 > 0.1) {
                if (!ORIENTATION) {
                    legCount_Enemy = legCount_Enemy + 1;
                } else {
                    legCount_Enemy_R = legCount_Enemy_R + 1;
                }
                t_Enemy = System.currentTimeMillis();
            }
            if (legCount_Enemy > 5) {
                legCount_Enemy = 0;
                STATE_Enemy = passive;
                impPhase_Enemy = false;
                kicked_Enemy = false;
            }
            if (legCount_Enemy_R > 5) {
                legCount_Enemy_R = 0;
                STATE_Enemy = passive;
                impPhase_Enemy = false;
                kicked_Enemy = false;
            }
        }

        if (STATE_Enemy == directleg) {
            if ((System.currentTimeMillis() - t_Enemy) / 1000 > 0.2) {
                if (!ORIENTATION) {
                    directlegCount_Enemy = directlegCount_Enemy + 1;
                } else {
                    directlegCount_Enemy_R = directlegCount_Enemy_R + 1;
                }
                t_Enemy = System.currentTimeMillis();
            }
            if (directlegCount_Enemy > 2) {
                directlegCount_Enemy = 0;
                STATE_Enemy = passive;
                impPhase_Enemy = false;
                kicked_Enemy = false;
            }
            if (directlegCount_Enemy_R > 2) {
                directlegCount_Enemy_R = 0;
                STATE_Enemy = passive;
                impPhase_Enemy = false;
                kicked_Enemy = false;
            }
        }

        if (STATE_Enemy == closeleg) {
            if ((System.currentTimeMillis() - t_Enemy) / 1000 > 0.18) {
                if (!ORIENTATION) {
                    closelegCount_Enemy = closelegCount_Enemy + 1;
                } else {
                    closelegCount_Enemy_R = closelegCount_Enemy_R + 1;
                }
                t_Enemy = System.currentTimeMillis();
            }
            if (closelegCount_Enemy > 2) {
                closelegCount_Enemy = 0;
                STATE_Enemy = passive;
                impPhase_Enemy = false;
                kicked_Enemy = false;
            }
            if (closelegCount_Enemy_R > 2) {
                closelegCount_Enemy_R = 0;
                STATE_Enemy = passive;
                impPhase_Enemy = false;
                kicked_Enemy = false;
            }
        }

        if (STATE_Enemy == wheel) {
            if ((System.currentTimeMillis() - t_Enemy) / 1000 > 0.15) {
                if (!ORIENTATION) {
                    wheelCount_Enemy = wheelCount_Enemy + 1;
                    x_Enemy = x_Enemy + 15;
                } else {
                    wheelCount_Enemy_R = wheelCount_Enemy_R + 1;
                    x_Enemy = x_Enemy - 15;
                }
                t_Enemy = System.currentTimeMillis();
            }
            if (wheelCount_Enemy > 7) {
                wheelCount_Enemy = 0;
                STATE_Enemy = passive;
                x_Enemy = x_Enemy + 25;
                impPhase_Enemy = false;
                kicked_Enemy = false;
            }
            if (wheelCount_Enemy_R > 7) {
                wheelCount_Enemy_R = 0;
                STATE_Enemy = passive;
                x_Enemy = x_Enemy + 25;
                impPhase_Enemy = false;
                kicked_Enemy = false;
            }
        }

        if (STATE_Enemy == death) {
            if ((System.currentTimeMillis() - t_Enemy) / 1000 > 0.05) {
                if (!ORIENTATION) {
                    x_Enemy = x_Enemy - 60;
                    y_Enemy = y_Enemy - 60;
                } else {
                    x_Enemy = x_Enemy + 60;
                    y_Enemy = y_Enemy - 60;
                }
                t_Enemy = System.currentTimeMillis();
            }
        }

        if (STATE_Enemy != death) {
            if (x_Enemy < 0) x_Enemy = 5;
            if (x_Enemy + 150 > 1280) x_Enemy = 1275 - 150;
        }
    }

    protected void catchCollisions() {

        if (ORIENTATION) {

            if (STATE == hand && handCount == 2) {
                if (Math.sqrt(Math.pow((x + 167) - (x_Enemy + 98), 2) + Math.pow((y + 52) - (y_Enemy + 52), 2)) < 35) {
                    healthFactor_Enemy++;
                    kicked = true;
                    mp.start();
                    vibrator.vibrate(30);
                }
            }

            if (STATE == directhand && directhandCount == 1) {
                if (Math.sqrt(Math.pow((x + 158) - (x_Enemy + 105), 2) + Math.pow((y + 97 - 4) - (y_Enemy + 85), 2)) <
                    43) {
                    healthFactor_Enemy++;
                    kicked = true;
                    mp.start();
                    vibrator.vibrate(30);
                }
            }

            if (STATE == closehand && closehandCount == 1) {
                if (Math.sqrt(Math.pow((x + 109) - (x_Enemy + 94), 2) + Math.pow((y + 30 - 4) - (y_Enemy + 37), 2)) <
                    42) {
                    healthFactor_Enemy++;
                    kicked = true;
                    mp.start();
                    vibrator.vibrate(30);
                }
            }

            if (STATE == leg && legCount == 3) {
                if (Math.sqrt(Math.pow((x + 157) - (x_Enemy + 94), 2) + Math.pow((y + 31 - 25) - (y_Enemy + 37), 2)) <
                    44) {
                    healthFactor_Enemy++;
                    kicked = true;
                    mp.start();
                    vibrator.vibrate(30);
                }
            }

            if (STATE == closeleg && closelegCount == 1) {
                if (Math.sqrt(Math.pow((x + 124) - (x_Enemy + 94), 2) + Math.pow((y + 85 - 27) - (y_Enemy + 37), 2)) <
                    46) {
                    healthFactor_Enemy++;
                    kicked = true;
                    mp.start();
                    vibrator.vibrate(30);
                }
            }

            if (STATE == directleg && directlegCount == 1) {
                if (Math.sqrt(Math.pow((x + 195) - (x_Enemy + 105), 2) + Math.pow((y + 86 - 20) - (y_Enemy + 85), 2)) <
                    54) {
                    healthFactor_Enemy++;
                    kicked = true;
                    mp.start();
                    vibrator.vibrate(30);
                }
            }

            if (STATE == wheel && wheelCount == 2) {
                if (Math.sqrt(Math.pow((x + 204) - (x_Enemy + 105), 2) + Math.pow((y + 85 - 50) - (y_Enemy + 85), 2)) <
                    55) {
                    healthFactor_Enemy++;
                    kicked = true;
                    mp.start();
                    vibrator.vibrate(30);
                }
            }
        } else {

            if (STATE == hand && handCount_R == 2) {
                if (Math.sqrt(Math.pow((x + 13) - (x_Enemy + 52), 2) + Math.pow((y + 52) - (y_Enemy + 52), 2)) < 35) {
                    healthFactor_Enemy++;
                    kicked = true;
                    mp.start();
                    vibrator.vibrate(30);
                }
            }

            if (STATE == directhand && directhandCount_R == 1) {
                if (Math.sqrt(Math.pow((x + 12) - (x_Enemy + 45), 2) + Math.pow((y + 97 - 4) - (y_Enemy + 85), 2)) <
                    43) {
                    healthFactor_Enemy++;
                    kicked = true;
                    mp.start();
                    vibrator.vibrate(30);
                }
            }

            if (STATE == closehand && closehandCount_R == 1) {
                if (Math.sqrt(Math.pow((x + 41) - (x_Enemy + 56), 2) + Math.pow((y + 30 - 4) - (y_Enemy + 37), 2)) <
                    42) {
                    healthFactor_Enemy++;
                    kicked = true;
                    mp.start();
                    vibrator.vibrate(30);
                }
            }

            if (STATE == leg && legCount_R == 3) {
                if (Math.sqrt(Math.pow((x + 23) - (x_Enemy + 56), 2) + Math.pow((y + 31 - 25) - (y_Enemy + 37), 2)) <
                    44) {
                    healthFactor_Enemy++;
                    kicked = true;
                    mp.start();
                    vibrator.vibrate(30);
                }
            }

            if (STATE == closeleg && closelegCount_R == 1) {
                if (Math.sqrt(Math.pow((x + 26) - (x_Enemy + 56), 2) + Math.pow((y + 85 - 27) - (y_Enemy + 37), 2)) <
                    46) {
                    healthFactor_Enemy++;
                    kicked = true;
                    mp.start();
                    vibrator.vibrate(30);
                }
            }

            if (STATE == directleg && directlegCount_R == 1) {
                if (Math.sqrt(Math.pow((x + 20) - (x_Enemy + 45), 2) + Math.pow((y + 86 - 20) - (y_Enemy + 85), 2)) <
                    54) {
                    healthFactor_Enemy++;
                    kicked = true;
                    mp.start();
                    vibrator.vibrate(30);
                }
            }

            if (STATE == wheel && wheelCount_R == 2) {
                if (Math.sqrt(Math.pow((x + 11) - (x_Enemy + 45), 2) + Math.pow((y + 85 - 50) - (y_Enemy + 85), 2)) <
                    55) {
                    healthFactor_Enemy++;
                    kicked = true;
                    mp.start();
                    vibrator.vibrate(30);
                }
            }
        }
    }

    protected void catchCollisions_Enemy() {

        if (ORIENTATION) {
            if (STATE_Enemy == hand && handCount_Enemy_R == 2) {
                if (Math.sqrt(Math.pow((x_Enemy + 13) - (x + 52), 2) + Math.pow((y_Enemy + 52) - (y + 52), 2)) < 35) {
                    healthFactor++;
                    kicked_Enemy = true;
                    mp.start();
                    vibrator.vibrate(30);
                }
            }

            if (STATE_Enemy == directhand && directhandCount_Enemy_R == 1) {
                if (Math.sqrt(Math.pow((x_Enemy + 12) - (x + 45), 2) + Math.pow((y_Enemy + 97 - 4) - (y + 85), 2)) <
                    43) {
                    healthFactor++;
                    kicked_Enemy = true;
                    mp.start();
                    vibrator.vibrate(30);
                }
            }

            if (STATE_Enemy == closehand && closehandCount_Enemy_R == 1) {
                if (Math.sqrt(Math.pow((x_Enemy + 41) - (x + 56), 2) + Math.pow((y_Enemy + 30 - 4) - (y + 37), 2)) <
                    42) {
                    healthFactor++;
                    kicked_Enemy = true;
                    mp.start();
                    vibrator.vibrate(30);
                }
            }

            if (STATE_Enemy == leg && legCount_Enemy_R == 3) {
                if (Math.sqrt(Math.pow((x_Enemy + 23) - (x + 56), 2) + Math.pow((y_Enemy + 31 - 25) - (y + 37), 2)) <
                    44) {
                    healthFactor++;
                    kicked_Enemy = true;
                    mp.start();
                    vibrator.vibrate(30);
                }
            }

            if (STATE_Enemy == closeleg && closelegCount_Enemy_R == 1) {
                if (Math.sqrt(Math.pow((x_Enemy + 26) - (x + 56), 2) + Math.pow((y_Enemy + 85 - 27) - (y + 37), 2)) <
                    46) {
                    healthFactor++;
                    kicked_Enemy = true;
                    mp.start();
                    vibrator.vibrate(30);
                }
            }

            if (STATE_Enemy == directleg && directlegCount_Enemy_R == 1) {
                if (Math.sqrt(Math.pow((x_Enemy + 20) - (x + 45), 2) + Math.pow((y_Enemy + 86 - 20) - (y + 85), 2)) <
                    54) {
                    healthFactor++;
                    kicked_Enemy = true;
                    mp.start();
                    vibrator.vibrate(30);
                }
            }

            if (STATE_Enemy == wheel && wheelCount_Enemy_R == 2) {
                if (Math.sqrt(Math.pow((x_Enemy + 11) - (x + 45), 2) + Math.pow((y_Enemy + 85 - 50) - (y + 85), 2)) <
                    55) {
                    healthFactor++;
                    kicked_Enemy = true;
                    mp.start();
                    vibrator.vibrate(30);
                }
            }
        } else {

            if (STATE_Enemy == hand && handCount_Enemy == 2) {
                if (Math.sqrt(Math.pow((x_Enemy + 167) - (x + 98), 2) + Math.pow((y_Enemy + 52) - (y + 52), 2)) < 35) {
                    healthFactor++;
                    kicked_Enemy = true;
                    mp.start();
                    vibrator.vibrate(30);
                }
            }

            if (STATE_Enemy == directhand && directhandCount_Enemy == 1) {
                if (Math.sqrt(Math.pow((x_Enemy + 158) - (x + 105), 2) + Math.pow((y_Enemy + 97 - 4) - (y + 85), 2)) <
                    43) {
                    healthFactor++;
                    kicked_Enemy = true;
                    mp.start();
                    vibrator.vibrate(30);
                }
            }

            if (STATE_Enemy == closehand && closehandCount_Enemy == 1) {
                if (Math.sqrt(Math.pow((x_Enemy + 109) - (x + 94), 2) + Math.pow((y_Enemy + 30 - 4) - (y + 37), 2)) <
                    42) {
                    healthFactor++;
                    kicked_Enemy = true;
                    mp.start();
                    vibrator.vibrate(30);
                }
            }

            if (STATE_Enemy == leg && legCount_Enemy == 3) {
                if (Math.sqrt(Math.pow((x_Enemy + 157) - (x + 94), 2) + Math.pow((y_Enemy + 31 - 25) - (y + 37), 2)) <
                    44) {
                    healthFactor++;
                    kicked_Enemy = true;
                    mp.start();
                    vibrator.vibrate(30);
                }
            }

            if (STATE_Enemy == closeleg && closelegCount_Enemy == 1) {
                if (Math.sqrt(Math.pow((x_Enemy + 124) - (x + 94), 2) + Math.pow((y_Enemy + 85 - 27) - (y + 37), 2)) <
                    46) {
                    healthFactor++;
                    kicked_Enemy = true;
                    mp.start();
                    vibrator.vibrate(30);
                }
            }

            if (STATE_Enemy == directleg && directlegCount_Enemy == 1) {
                if (Math.sqrt(Math.pow((x_Enemy + 195) - (x + 105), 2) + Math.pow((y_Enemy + 86 - 20) - (y + 85), 2)) <
                    54) {
                    healthFactor++;
                    kicked_Enemy = true;
                    mp.start();
                    vibrator.vibrate(30);
                }
            }

            if (STATE_Enemy == wheel && wheelCount_Enemy == 2) {
                if (Math.sqrt(Math.pow((x_Enemy + 204) - (x + 105), 2) + Math.pow((y_Enemy + 85 - 50) - (y + 85), 2)) <
                    55) {
                    healthFactor++;
                    kicked_Enemy = true;
                    mp.start();
                    vibrator.vibrate(30);
                }
            }
        }
    }

    protected void generator() {

        if (healthFactor_Enemy < 22) {

            if (Math.abs(x - x_Enemy) > 90) {
                generator = r.nextInt(80);

                switch (generator) {
                    case 5: {
                        STATE_Enemy = step;
                        impPhase_Enemy = true;
                        break;
                    }
                    case 50: {
                        STATE_Enemy = step;
                        impPhase_Enemy = true;
                        break;
                    }
                    case 6: {
                        STATE_Enemy = wheel;
                        impPhase_Enemy = true;
                        break;
                    }
                }
            } else {
                generator = r.nextInt(200);
                switch (generator) {
                    case 5: {
                        STATE_Enemy = hand;
                        impPhase_Enemy = true;
                        break;
                    }
                    case 50: {
                        STATE_Enemy = closehand;
                        impPhase_Enemy = true;
                        break;
                    }
                    case 6: {
                        STATE_Enemy = directhand;
                        impPhase_Enemy = true;
                        break;
                    }
                    case 100: {
                        STATE_Enemy = leg;
                        impPhase_Enemy = true;
                        break;
                    }
                    case 150: {
                        STATE_Enemy = closeleg;
                        impPhase_Enemy = true;
                        break;
                    }
                    case 170: {
                        STATE_Enemy = directleg;
                        impPhase_Enemy = true;
                        break;
                    }
                }
            }
        } else {
            if (Math.abs(x - x_Enemy) < 90) {
                generator = r.nextInt(150);
                switch (generator) {
                    case 5: {
                        STATE_Enemy = dodge;
                        impPhase_Enemy = true;
                        break;
                    }
                    case 50: {
                        STATE_Enemy = stepback;
                        impPhase_Enemy = true;
                        break;
                    }
                    case 90: {
                        STATE_Enemy = stepback;
                        impPhase_Enemy = true;
                        break;
                    }
                    case 6: {
                        STATE_Enemy = directleg;
                        impPhase_Enemy = true;
                        break;
                    }
                    case 100: {
                        STATE_Enemy = directhand;
                        impPhase_Enemy = true;
                        break;
                    }
                    case 140: {
                        STATE_Enemy = hand;
                        impPhase_Enemy = true;
                        break;
                    }
                    case 135: {
                        STATE_Enemy = closehand;
                        impPhase_Enemy = true;
                        break;
                    }
                    case 131: {
                        STATE_Enemy = closeleg;
                        impPhase_Enemy = true;
                        break;
                    }
                    case 125: {
                        STATE_Enemy = leg;
                        impPhase_Enemy = true;
                        break;
                    }
                }
            } else {
                generator = r.nextInt(160);
                switch (generator) {
                    case 50: {
                        STATE_Enemy = stepback;
                        impPhase_Enemy = true;
                        break;
                    }
                    case 150: {
                        STATE_Enemy = stepback;
                        impPhase_Enemy = true;
                        break;
                    }
                    case 70: {
                        STATE_Enemy = step;
                        impPhase_Enemy = true;
                        break;
                    }
                }
            }
        }
    }

    protected void updatePhases(String s) {
        if (ORIENTATION) {
            if (s == passive) {
                stepCount = 0;
                stepbackCount = 0;
                dodgeCount = 0;
                handCount = 0;
                closehandCount = 0;
                directhandCount = 0;
                legCount = 0;
                directlegCount = 0;
                closelegCount = 0;
                wheelCount = 0;
            }
            if (s == step) {
                passiveCount = 0;
                stepbackCount = 0;
                dodgeCount = 0;
                handCount = 0;
                closehandCount = 0;
                directhandCount = 0;
                legCount = 0;
                directlegCount = 0;
                closelegCount = 0;
                wheelCount = 0;
            }
            if (s == stepback) {
                stepCount = 0;
                passiveCount = 0;
                dodgeCount = 0;
                handCount = 0;
                closehandCount = 0;
                directhandCount = 0;
                legCount = 0;
                directlegCount = 0;
                closelegCount = 0;
                wheelCount = 0;
            }
            if (s == dodge) {
                stepCount = 0;
                passiveCount = 0;
                stepbackCount = 0;
                handCount = 0;
                closehandCount = 0;
                directhandCount = 0;
                legCount = 0;
                directlegCount = 0;
                closelegCount = 0;
                wheelCount = 0;
            }
            if (s == hand) {
                stepCount = 0;
                passiveCount = 0;
                stepbackCount = 0;
                dodgeCount = 0;
                closehandCount = 0;
                directhandCount = 0;
                legCount = 0;
                directlegCount = 0;
                closelegCount = 0;
                wheelCount = 0;
            }
            if (s == closehand) {
                stepCount = 0;
                passiveCount = 0;
                stepbackCount = 0;
                dodgeCount = 0;
                handCount = 0;
                directhandCount = 0;
                legCount = 0;
                directlegCount = 0;
                closelegCount = 0;
                wheelCount = 0;
            }
            if (s == directhand) {
                stepCount = 0;
                passiveCount = 0;
                stepbackCount = 0;
                dodgeCount = 0;
                handCount = 0;
                closehandCount = 0;
                legCount = 0;
                directlegCount = 0;
                closelegCount = 0;
                wheelCount = 0;
            }
            if (s == leg) {
                stepCount = 0;
                passiveCount = 0;
                stepbackCount = 0;
                dodgeCount = 0;
                handCount = 0;
                closehandCount = 0;
                directhandCount = 0;
                directlegCount = 0;
                closelegCount = 0;
                wheelCount = 0;
            }
            if (s == directleg) {
                stepCount = 0;
                passiveCount = 0;
                stepbackCount = 0;
                dodgeCount = 0;
                handCount = 0;
                closehandCount = 0;
                legCount = 0;
                directhandCount = 0;
                closelegCount = 0;
                wheelCount = 0;
            }
            if (s == closeleg) {
                stepCount = 0;
                passiveCount = 0;
                stepbackCount = 0;
                dodgeCount = 0;
                handCount = 0;
                closehandCount = 0;
                directhandCount = 0;
                legCount = 0;
                directlegCount = 0;
                wheelCount = 0;
            }
            if (s == wheel) {
                stepCount = 0;
                passiveCount = 0;
                stepbackCount = 0;
                dodgeCount = 0;
                handCount = 0;
                closehandCount = 0;
                directhandCount = 0;
                legCount = 0;
                directlegCount = 0;
                closelegCount = 0;
            }
        } else {
            if (s == passive) {
                stepCount_R = 0;
                stepbackCount_R = 0;
                dodgeCount_R = 0;
                handCount_R = 0;
                closehandCount_R = 0;
                directhandCount_R = 0;
                legCount_R = 0;
                directlegCount_R = 0;
                closelegCount_R = 0;
                wheelCount_R = 0;
            }
            if (s == step) {
                passiveCount_R = 0;
                stepbackCount_R = 0;
                dodgeCount_R = 0;
                handCount_R = 0;
                closehandCount_R = 0;
                directhandCount_R = 0;
                legCount_R = 0;
                directlegCount_R = 0;
                closelegCount_R = 0;
                wheelCount_R = 0;
            }
            if (s == stepback) {
                stepCount_R = 0;
                passiveCount_R = 0;
                dodgeCount_R = 0;
                handCount_R = 0;
                closehandCount_R = 0;
                directhandCount_R = 0;
                legCount_R = 0;
                directlegCount_R = 0;
                closelegCount_R = 0;
                wheelCount_R = 0;
            }
            if (s == dodge) {
                stepCount_R = 0;
                passiveCount_R = 0;
                stepbackCount_R = 0;
                handCount_R = 0;
                closehandCount_R = 0;
                directhandCount_R = 0;
                legCount_R = 0;
                directlegCount_R = 0;
                closelegCount_R = 0;
                wheelCount_R = 0;
            }
            if (s == hand) {
                stepCount_R = 0;
                passiveCount_R = 0;
                stepbackCount_R = 0;
                dodgeCount_R = 0;
                closehandCount_R = 0;
                directhandCount_R = 0;
                legCount_R = 0;
                directlegCount_R = 0;
                closelegCount_R = 0;
                wheelCount_R = 0;
            }
            if (s == closehand) {
                stepCount_R = 0;
                passiveCount_R = 0;
                stepbackCount_R = 0;
                dodgeCount_R = 0;
                handCount_R = 0;
                directhandCount_R = 0;
                legCount_R = 0;
                directlegCount_R = 0;
                closelegCount_R = 0;
                wheelCount_R = 0;
            }
            if (s == directhand) {
                stepCount_R = 0;
                passiveCount_R = 0;
                stepbackCount_R = 0;
                dodgeCount_R = 0;
                handCount_R = 0;
                closehandCount_R = 0;
                legCount_R = 0;
                directlegCount_R = 0;
                closelegCount_R = 0;
                wheelCount_R = 0;
            }
            if (s == leg) {
                stepCount_R = 0;
                passiveCount_R = 0;
                stepbackCount_R = 0;
                dodgeCount_R = 0;
                handCount_R = 0;
                closehandCount_R = 0;
                directhandCount_R = 0;
                directlegCount_R = 0;
                closelegCount_R = 0;
                wheelCount_R = 0;
            }
            if (s == directleg) {
                stepCount_R = 0;
                passiveCount_R = 0;
                stepbackCount_R = 0;
                dodgeCount_R = 0;
                handCount_R = 0;
                closehandCount_R = 0;
                legCount_R = 0;
                directhandCount_R = 0;
                closelegCount_R = 0;
                wheelCount_R = 0;
            }
            if (s == closeleg) {
                stepCount_R = 0;
                passiveCount_R = 0;
                stepbackCount_R = 0;
                dodgeCount_R = 0;
                handCount_R = 0;
                closehandCount_R = 0;
                directhandCount_R = 0;
                legCount_R = 0;
                directlegCount_R = 0;
                wheelCount_R = 0;
            }
            if (s == wheel) {
                stepCount_R = 0;
                passiveCount_R = 0;
                stepbackCount_R = 0;
                dodgeCount_R = 0;
                handCount_R = 0;
                closehandCount_R = 0;
                directhandCount_R = 0;
                legCount_R = 0;
                directlegCount_R = 0;
                closelegCount_R = 0;
            }
        }
    }

    private void theEnd() {
        Intent intent = new Intent(context, Game_Menu.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
        System.exit(0);
    }

    public float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return dp * (metrics.densityDpi / 160f);
    }

    private class MyScaleGestureListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {

        public boolean onScale(ScaleGestureDetector scaleGestureDetector) {

            float scaleFactor = scaleGestureDetector.getScaleFactor();
            float focusX = scaleGestureDetector.getFocusX();
            float focusY = scaleGestureDetector.getFocusY();

            if (mScaleFactor * scaleFactor > 1 && mScaleFactor * scaleFactor < 2) {
                mScaleFactor *= scaleGestureDetector.getScaleFactor();
                canvasSize = viewSize * mScaleFactor;

                int scrollX = (int) ((getScrollX() + focusX) * scaleFactor - focusX);
                scrollX = Math.min(Math.max(scrollX, 0), (int) canvasSize - viewSize);
                int scrollY = (int) ((getScrollY() + focusY) * scaleFactor - focusY);
                scrollY = Math.min(Math.max(scrollY, 0), (int) canvasSize - viewSize);
                scrollTo(scrollX, scrollY);
            }

            invalidate();
            return true;
        }
    }

    private class MyGestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            //hor
            if (getScrollX() + distanceX < canvasSize - viewSize && getScrollX() + distanceX > 0) {
                scrollBy((int) distanceX, 0);
            }
            //vert
            if (getScrollY() + distanceY < canvasSize - viewSize && getScrollY() + distanceY > 0) {
                scrollBy(0, (int) distanceY);
            }
            return true;
        }

        @Override
        public boolean onDoubleTapEvent(MotionEvent event) {
            if (theEnd) theEnd();
            mScaleFactor = 1f;
            canvasSize = viewSize;
            scrollTo(0, 0);
            invalidate();
            return true;
        }
    }
}
