package com.example.shadowfight;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;

public class Splash extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        MediaPlayer mp = MediaPlayer.create(this, R.raw.intro);
        mp.start();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {

            public void run() {
                startActivity(new Intent(Splash.this, Game_Menu.class));
                finish();
            }
        }, 2000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_splash, menu);
        return true;
    }
}
