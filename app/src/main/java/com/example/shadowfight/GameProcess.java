package com.example.shadowfight;

import android.app.Activity;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.Menu;

public class GameProcess extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        Entities entity = new Entities(this, null, vibrator);
        MediaPlayer mpFight = MediaPlayer.create(this, R.raw.mkfight);
        MediaPlayer mpBell = MediaPlayer.create(this, R.raw.mkbell);
        MediaPlayer mpBG = MediaPlayer.create(this, R.raw.menu_music);

        setContentView(entity);

        mpFight.start();
        mpBell.start();
        mpBG.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_game_process, menu);
        return true;
    }
}
